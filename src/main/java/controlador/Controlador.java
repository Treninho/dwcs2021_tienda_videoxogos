package controlador;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import modelo.CarritoProductosDAO;
import modelo.ClienteDAO;
import modelo.ProductoDAO;
import modelo.RolesDAO;

/**
 * Servlet implementation class Controlador
 */
@WebServlet("/controlador")
public class Controlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private  int idCarro;
	private DataSource pool;
	private RequestDispatcher requestDispatcher;
	private ClienteDAO clienteDAO;
	private ProductoDAO productoDAO;
	private CarritoProductosDAO carritoDAO;
	private RolesDAO rolesDAO;
	
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	
    public Controlador() {
        super();
       
        
    }

    
    @Override
	public void init() throws ServletException {	
		super.init();
		try {
				
            InitialContext ctx = new InitialContext();
    		pool = (DataSource)ctx.lookup("java:comp/env/jdbc/dwcs_tienda_videoxogos");
    		clienteDAO = new ClienteDAO(pool);
    		productoDAO = new ProductoDAO(pool);
    		carritoDAO = new CarritoProductosDAO(pool);
    		rolesDAO = new RolesDAO(pool);
		} catch (Exception e) {
		     throw new ServletException(e);
		}
    
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	

		doPost(request,response);
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		
		
		// Obtenemos el botón de la vista
        String boton = request.getParameter("comando");
    

        // Si hemos pulsado el botón, entra en el switch
      if (boton!=null) {
    	  switch(boton) {
          
    	 
    	  
    	  
    	
    	 // Si el usuario quiere cambiar password
    	  case "changePassword":
    		  	String emailCliente = request.getParameter("correo");
    		  	String passCliente = request.getParameter("passwordCliente");
    		  	passCliente = clienteDAO.cifrarContrasinal(passCliente);
			try {
				clienteDAO.cambiarContrasinal(emailCliente, passCliente);
				System.out.println("Contrasinal cambiada");
				request.setAttribute("lista", productoDAO.getAllProductos());
				request.setAttribute("listaCarrito", carritoDAO.getCarrito());
				request.getRequestDispatcher("/usuarioProductos.jsp").forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
    		 break;
    		 // ASIGNAR ROL A CLIENTE
    	  case "asignar":
    		  String emailRol = request.getParameter("emailRol");
    		  String rol = request.getParameter("roles");
			try {
				rolesDAO.asignarRol(emailRol, emailRol);
				request.getRequestDispatcher("/listadoProductos.jsp").forward(request, response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	  break;
    	// CUANDO PULSAMOS BOTON LOGOUT
    	  case "logout":
    		  request.getSession().removeAttribute("user");  // eliminamos atributo de la session 
    		  request.getSession().removeAttribute("rol");   // elimanos atributo rol de la session
    		  request.getRequestDispatcher("/login.jsp").forward(request, response);
    		  
    		 break;
    		 // CUANDO PULSAMOS BOTON REGISTRO NOS REENVIA AL FORMULARIO DE REGISTRO
	        case "registro":
	        		request.getRequestDispatcher("/registro.jsp").forward(request, response);
	        		// métodos..
	        break;
	        	// CUANDO ENVIAMOS EL REGISTRO, SE INSERTAN DATOS EN LA BASE DE DATOS DEL CLIENTE Y SE CIFRA CONTRASEÑA
	        case "enviar_registro":
	        	String nomeC = request.getParameter("nome");
	        	String ape = request.getParameter("apelidos");
	        	String dire = request.getParameter("direccion");
	        	String email = request.getParameter("email");
	        	String pass = request.getParameter("password");
	        	pass = clienteDAO.cifrarContrasinal(pass);
			try {
				clienteDAO.insertarCliente(nomeC, ape, dire, email, pass);
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	        	request.getRequestDispatcher("/login.jsp").forward(request, response);
	        	
	        	// métodos..
	        break;  
	        
	        // CUANDO PULSAMOS BOTON ELIMINAR DE UN PRODUCTO
	        case "eliminar":
	        			
					try {
						int codigoProducto = Integer.parseInt(request.getParameter("id"));
						
						productoDAO.deleteProducto(codigoProducto, codigoProducto);
						request.setAttribute("lista", productoDAO.getAllProductos());
						request.setAttribute("clientes", clienteDAO.getAllClientes());
						request.getRequestDispatcher("/listadoProductos.jsp").forward(request, response);
					} catch (NumberFormatException e1) {
						System.out.println("Erro! ");
						e1.printStackTrace();
					} catch (Exception e1) {
						
						e1.printStackTrace();
					}	
			break;
			
			// INSERTAR NUEVO PRODUCTO CON VALORES DE LOS INPUT
	        case "insertar":
	        	String nome = request.getParameter("nProducto");
	        	double prezo = Double.parseDouble(request.getParameter("prezo"));
	        	int stock = Integer.parseInt(request.getParameter("stock"));
	        	String tipo = request.getParameter("tipo");
	        	int descuento = Integer.parseInt(request.getParameter("descuento"));
			try {
					productoDAO.insertarProducto(nome, prezo, stock, tipo, descuento);
					request.setAttribute("lista", productoDAO.getAllProductos());
					request.setAttribute("clientes", clienteDAO.getAllClientes());
					//request.getRequestDispatcher("/listadoProductos.jsp").forward(request, response);
					response.sendRedirect("controlador");  // para que no guarde la lista para que no se duplique
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       break;
	       
	       // al darle al confirmar edicion editamos el producto seleccionando su id
	        case "confirm":
	        	//int codigoEdit = Integer.parseInt(request.getParameter("id"));
	        	int codigo = Integer.parseInt(request.getParameter("codigoEdit"));
	        	String nomeEdit = request.getParameter("productoEdit");
	        	double prezoEdit = Double.parseDouble(request.getParameter("prezoEdit"));
	        	int stockEdit = Integer.parseInt(request.getParameter("stockEdit"));
	        	String tipoEdit = request.getParameter("tipoEdit");
	        	int descuentoEdit = Integer.parseInt(request.getParameter("descuentoEdit"));
	        	
			try {
				productoDAO.editarProducto(codigo,nomeEdit, prezoEdit, stockEdit, tipoEdit, descuentoEdit);
				request.setAttribute("lista", productoDAO.getAllProductos());
				request.setAttribute("clientes", clienteDAO.getAllClientes());
				request.getRequestDispatcher("/listadoProductos.jsp").forward(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
			
			// añadimos al carrito el id, prezo y nome del producto
	        case "carrito":
	        	int codigoP = Integer.parseInt(request.getParameter("id"));
	        	Double importe = Double.parseDouble(request.getParameter("prezo"));
	        	String nomeP =request.getParameter("nome");
			try {
				productoDAO.putCarrito(codigoP,importe, nomeP);
				try {
					request.setAttribute("listaCarrito", carritoDAO.getCarrito());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					request.setAttribute("lista", productoDAO.getAllProductos());
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				request.getRequestDispatcher("/usuarioProductos.jsp").forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
			// quitamos un prodcuto eliminando por el id del carrito
	        case "quitar":
	        	
			try {
				carritoDAO.deleteCarrito(Integer.parseInt(request.getParameter("idCarrito")));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				request.setAttribute("lista", productoDAO.getAllProductos());
				request.setAttribute("listaCarrito", carritoDAO.getCarrito());
				request.getRequestDispatcher("/usuarioProductos.jsp").forward(request, response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			break;
			// eliminar cliente por su id
	        case "deleteCliente":
	        	int codCliente = Integer.parseInt(request.getParameter("idCliente"));
	        	String emailC = request.getParameter("emailCliente");
			try {
				clienteDAO.deleteCliente(codCliente, emailC);
				request.setAttribute("lista", productoDAO.getAllProductos());
				request.setAttribute("clientes", clienteDAO.getAllClientes());
				request.getRequestDispatcher("/listadoProductos.jsp").forward(request, response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	        break;

			// si no llega a ningún caso, manda la lista de productos y clientes
	        default:
	        	try {
	       		  	request.setAttribute("lista", productoDAO.getAllProductos());
	    			request.setAttribute("clientes", clienteDAO.getAllClientes());
	    			
	    			// si los roles de los usuarios son roles de admin o user, los redirije a su página
	       		  if (request.getSession().getAttribute("rol").equals("ADMIN")) {
	       			request.getRequestDispatcher("/listadoProductos.jsp").forward(request, response);
	       		  }else if (request.getSession().getAttribute("rol").equals("USER")){
	       				request.setAttribute("listaCarrito", carritoDAO.getCarrito());
	       				request.getRequestDispatcher("/usuarioProductos.jsp").forward(request, response);
	       			 }else {
	       				 request.getRequestDispatcher("/login.jsp").forward(request, response);
	       			 }
	       		} catch (Exception e) {
	       			e.printStackTrace();
	       		}
	        	break;
    	  } 
    	  // Si no entra en el switch, carga la lista de Productos y clientes y los devuelve
     }else {
    		try {
       		  	request.setAttribute("lista", productoDAO.getAllProductos());
    			request.setAttribute("clientes", clienteDAO.getAllClientes());
    			
    			// si la session el usuario es admin, va a su página de admin y si es user a su pagina de user
       		  if (request.getSession().getAttribute("rol").equals("ADMIN")) {
       			request.getRequestDispatcher("/listadoProductos.jsp").forward(request, response);
       			
       		  }else if (request.getSession().getAttribute("rol").equals("USER")){
       				request.setAttribute("listaCarrito", carritoDAO.getCarrito());
       				request.getRequestDispatcher("/usuarioProductos.jsp").forward(request, response);
       			 }else {
       				 request.getRequestDispatcher("/login.jsp").forward(request, response);
       			 }
       		} catch (Exception e) {
       			e.printStackTrace();
       		}
	  }
   }
       
}
