package controlador;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import modelo.CarritoProductosDAO;
import modelo.Cliente;
import modelo.ClienteDAO;
import modelo.ProductoDAO;

/**
 * Servlet implementation class ControlLogin
 */
@WebServlet("/login")
public class ControlLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
      private DataSource pool;
      private ClienteDAO clienteDAO;
      private Cliente cliente;
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	  @Override
		public void init() throws ServletException {	
			super.init();
			try {
					
	            InitialContext ctx = new InitialContext();
	    		pool = (DataSource)ctx.lookup("java:comp/env/jdbc/dwcs_tienda_videoxogos");
	    		clienteDAO = new ClienteDAO(pool);
			} catch (Exception e) {
			     throw new ServletException(e);
			}
	    
	    }
	
	
    public ControlLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
		// recibimos si ha pulsado algun boton con nombre de comando
		String comando = request.getParameter("comando");
		try {
		String destino = "/login.jsp";
		// recogemos valores del login
		String login = request.getParameter("email");
		String password = request.getParameter("password");
		
		if (login != null && password != null && login!="" && password!="") {
		switch(comando) {

		// si le damos a login
		case "login":
			// creamos cliente con la password y email que ha entrado el user
				Cliente cliente;
					cliente = clienteDAO.getCliente(login, password);
		// si el cliente existe y tiene rol ADMIN le damos atributo de Nome y de su Rol
		// y lo reenviamos al controlador donde lo mandará a la página de admin
			    if (cliente != null) {
			        if(cliente.getRol().equals("ADMIN")) {
			            request.getSession().setAttribute("user", cliente.getNome());
			            request.getSession().setAttribute("rol", cliente.getRol());
			            destino="/controlador";
			        }
			        // si es USER le mandamos atributos de USER de rol y lo mandamos al controlador
			        // donde si es User lo manda a la pagina de user
			        if (cliente.getRol().equals("USER")) {
			        	 request.getSession().setAttribute("user", cliente.getNome());
				            request.getSession().setAttribute("rol", cliente.getRol());
				            destino="/controlador";
			        }
			   // Pero si no es ni Admin ni user lo mandamos otra vez al login
			    }else {
		        destino = "/login.jsp";
					}
			
			break;
			// si le damos al registro, nos reenvia al formulario de registro
		case "registro":
			destino="/registro.jsp";
	}
		}
		request.getRequestDispatcher(destino).forward(request, response);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	
	}

	}

