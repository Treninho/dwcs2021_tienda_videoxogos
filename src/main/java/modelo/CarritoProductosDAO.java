package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

/**
 * CarritoDAO donde busca en la base de datos
 * @author Trenas
 *
 */
public class CarritoProductosDAO {

	/**
	 * Atributos de carritoDAO
	 */
	private  ArrayList<CarritoProductos> carrito;
	private  DataSource pool;
	
	/**
	 * Constructor 
	 * @param pool conection a la base de datos
	 */
	public CarritoProductosDAO(DataSource pool) {
		this.pool = pool;
	}
	
	/**
	 * Devuelve los productos del carrito actualmente
	 * @return carrito
	 * @throws Exception si no devuelve nada de la BDD
	 */
	public ArrayList<CarritoProductos> getCarrito() throws Exception{
		
		Connection c = pool.getConnection();
		carrito = new ArrayList<CarritoProductos>();
		PreparedStatement sentencia = c.prepareStatement("SELECT * FROM carrito_productos");
		ResultSet  rs= sentencia.executeQuery();
		while(rs.next()) {
			CarritoProductos carro = new CarritoProductos(rs.getInt("codigo_carrito"),rs.getDouble("importe"), rs.getString("nombre"));
			carrito.add(carro);
		}
		rs.close();
		c.close();
		return carrito;
	}
		/**
		 * Elimina carrito a partir del codigo del producto
		 * @param codigo codigo del carro para eliminar
		 * @throws SQLException si no devuelve nada de la base de datos
		 */
		public void deleteCarrito(int codigo) throws SQLException {
			Connection c = pool.getConnection();
			CallableStatement  miSentencia=c.prepareCall("{call DELETE_CARRITO(?)}"); 
			miSentencia.setInt(1, codigo);
			miSentencia.execute();
			miSentencia.close();
			c.close();
		}
		
		
		
		/*Connection c = pool.getConnection();
		carrito = new ArrayList<CarritoProductos>();
		CallableStatement  miSentencia=c.prepareCall("{call GET_CARRITO()}"); 
		miSentencia.execute();
		miSentencia.close();
		c.close();
		return carrito;
		*/
	}
	

