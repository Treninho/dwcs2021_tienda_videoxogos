package modelo;

/**
 * 
 * @author Trenas
 *Clase Roles
 */
public class Roles {

	/**
	 * Atributos de Roles
	 */
	private String rol;
	private String email;
	
	/**
	 * Constructor de Roles
	 * @param rol
	 * @param email
	 */
	public Roles(String rol, String email) {
		super();
		this.rol = rol;
		this.email = email;
	}
	/**
	 * Devuelve el Rol
	 * @return rol
	 */
	public String getRol() {
		return rol;
	}
	/**
	 * Cambia el rol
	 * @param rol
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}
	/**
	 * Devuelve el Email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Cambia el email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Genera informacion de la clase
	 */
	@Override
	public String toString() {
		return "Roles [rol=" + rol + ", email=" + email + "]";
	}
	
	
}
