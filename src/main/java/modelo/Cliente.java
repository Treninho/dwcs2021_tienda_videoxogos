package modelo;

/**
 * 
 * @author Trenas
 * Clase Cliente
 */
public class Cliente {

	
	/**
	 * Atributos clase Cliente
	 */
	private int codigoCliente;
	private String nome;
	private String apelidos;
	private String direccion;
	private String email;
	private int password;
	private String rol;
	
	
	/**
	 * Devuelve el rol
	 * @return rol
	 */
	public String getRol() {
		return rol;
	}
	/**
	 * Cambia el rol
	 * @param rol Cambia el rol
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}
	/**
	 * Constructor de Cliente
	 * @param codigo_cliente codigo del cliente
	 * @param nome nome cliente
	 * @param apelidos apelidos cliente	
	 * @param direccion direccion cliente
	 * @param email email cliente
	 */
	public Cliente(int codigo_cliente, String nome, String apelidos, String direccion, String email) {
		super();
		this.codigoCliente = codigo_cliente;
		this.nome = nome;
		this.apelidos = apelidos;
		this.direccion = direccion;
		this.email = email;
	}
	
	public Cliente(int codigo_cliente, String nome, String apelidos, String direccion, String email, String rol) {
		super();
		this.codigoCliente = codigo_cliente;
		this.nome = nome;
		this.apelidos = apelidos;
		this.direccion = direccion;
		this.email = email;
		this.rol = rol;
	}
	
	/**
	 * 
	 * @return codigoCliente Devuelve el codigo del Cliente
	 */
	public int getCodigoCliente() {
		return codigoCliente;
	}
	/**
	 * Cambia el codigo del Cliente
	 * @param getCodigoCliente El codigo del Cliente
	 */
	public void setgetCodigoCliente(int getCodigoCliente) {
		this.codigoCliente = getCodigoCliente;
	}
	/**
	 * Devuelve el nome del Cliente
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Cambia el nombre del cliente
	 * @param nome Cambia el nombre del cliente
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Devuelve los apellidos
	 * @return apelidos
	 */
	public String getApelidos() {
		return apelidos;
	}
	/**
	 * Cambia los apelidos
	 * @param apelidos Cambia os apelidos
	 */
	public void setApelidos(String apelidos) {
		this.apelidos = apelidos;
	}
	/**
	 * Devuelve la direccion
	 * @return direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * 
	 * @param direccion Cambia la direccion
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * Devuelve el emails
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 
	 * @param email Cambia el email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Devuelve la constrasinal
	 * @return password
	 */
	public int getPassword() {
		return password;
	}
	/**
	 * 
	 * @param password Cambia la contrasinal
	 */
	public void setPassword(int password) {
		this.password = password;
	}
/**
 * Genera el ToString
 */
	@Override
	public String toString() {
		return "Cliente [codigo_cliente=" + codigoCliente + ", nome=" + nome + ", apelidos=" + apelidos
				+ ", direccion=" + direccion + ", email=" + email + ", password=" + password + "]";
	}
	
	
	
	
}
