package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.BCrypt.Hasher;



/**
 * 
 * @author Trenas
 * Clase cliente
 */
public class ClienteDAO {
	
	/**
	 * Atributos de la clase Cliente
	 */
	private  DataSource pool;
	private  ArrayList<Cliente> clientes;
	private Cliente cliente;
	private String rol;
	
	/**
	 * Devuelve el rol
	 * @return rol
	 */
	public String getRol() {
		return rol;
	}
	/**
	 * Cambia el rol
	 * @param rol Cambia el rol
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}

	/**
	 * Constructor de ClienteDAO, se crea con el pool
	 * @param pool conection a bbdd
	 */
	public ClienteDAO(DataSource pool) {
		this.pool = pool;
	}

	
/**
 *	Devuelve un ArrayList de Cliente 
 * @return clientes
 * @throws Exception Si no recibimos clientes de la BDD
 */
	
	public  ArrayList<Cliente> getAllClientes() throws Exception {
		Connection c = pool.getConnection();
		clientes = new ArrayList<Cliente>();
		PreparedStatement sentencia = c.prepareStatement("SELECT * FROM cliente");
		ResultSet  rs= sentencia.executeQuery();
		while(rs.next()) {
			Cliente cliente = new Cliente(rs.getInt("codigo_cliente"), rs.getString("nome"), rs.getString("apelidos"), 
					rs.getString("direccion"), rs.getString("email"));
			clientes.add(cliente);
		}
		return clientes;
	}
	
	/**
	 * Elimina un cliente a partir de su codigo y su email
	 * @param codigo id del cliente
	 * @param email Email del cliente
	 * @throws SQLException Si no puede eliminar cliente
	 */
	public void deleteCliente(int codigo, String email) throws SQLException {
		Connection c = pool.getConnection();
		CallableStatement sentencia = c.prepareCall("{call DEL_ROL(?)}");
		sentencia.setString(1, email);
		sentencia.execute();
		sentencia.close();
		
		CallableStatement  miSentencia=c.prepareCall("{call DELETE_CLIENTE(?)}"); 
		miSentencia.setInt(1, codigo);
		miSentencia.execute();
		miSentencia.close();
		c.close();
		
	}
	
	/**
	 * Inserta un cliente nuevo
	 * @param nome  Inserta nome
	 * @param ape 	Inserta apelidos
	 * @param dire Inserta direccion
	 * @param email Inserta email
	 * @param pass Inserta password
	 * @throws SQLException Si no inserta cliente lanza exception
	 */
	public void insertarCliente(String nome, String ape, String dire, String email, String pass) throws SQLException {
			
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call INSERTAR_CLIENTE(?,?,?,?,?)}"); 
		miSentencia.setString(1, nome);
		miSentencia.setString(2, ape);
		miSentencia.setString(3, dire);
		miSentencia.setString(4, email);
		miSentencia.setString(5, pass);
		
		miSentencia.execute();
		miSentencia.close();
		c.close();
		
		}
	
	/**
	 * Edita un cliente nuevo
	 * @param codigo Edita codigo
	 * @param nome 	Edita nombre
	 * @param ape	Edita apelidos
	 * @param dire Edita direccion
	 * @param email Edita email
	 * @param password Edita password
	 * @throws SQLException lanza exception
	 */
	public void editCliente(int codigo, String nome, String ape, String dire, String email, String password) throws SQLException {
		
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call EDITAR_CLIENTE(?,?,?,?,?,?)}"); 
		miSentencia.setInt(1, codigo);
		miSentencia.setString(2, nome);
		miSentencia.setString(3, ape);
		miSentencia.setString(4, dire);
		miSentencia.setString(5, email);
		miSentencia.setString(6, password);
		
		miSentencia.execute();
		miSentencia.close();
		c.close();
	}
	
	/**
	 * Devuelve todos los clientes con su login y password
	 * @param login Email de login
	 * @param password Constrasinal del login
	 * @return Cliente devuelve un cliente
	 * @throws SQLException si no recibe ningun cliente
	 */
	public Cliente getCliente(String login, String password) throws SQLException {
		
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call GET_CLIENTE(?,?)}"); 
		miSentencia.setString(1, login);
		miSentencia.setString(2, password);
		ResultSet  rs= miSentencia.executeQuery();
		cliente = null;
		while(rs.next()) {
			cliente = new Cliente(rs.getInt("codigo_cliente"), rs.getString("nome"), rs.getString("apelidos"), 
					rs.getString("direccion"), rs.getString("email"), rs.getString("rol"));
		}
		rs.close();
		miSentencia.close();
		c.close();
		return cliente;
		
	}
	/**
	 * Cifra la contrasinal del cliente en el registro
	 * @param password Contrasinal 
	 * @return hashedPassword Contrasinal cifrada
	 */
	public String cifrarContrasinal(String password) {
        char[]  charPassword = password.toCharArray();
        Hasher hasher = BCrypt.withDefaults();
        String hashedPassword = hasher.hashToString(12, charPassword);
        return hashedPassword;
	}
	/**
	 * Cambia contrasinal de usuario
	 * @param email email usuario
	 * @param password Contrasinal usuario
	 * @throws SQLException lanza sqlException
	 */
	public void cambiarContrasinal(String email, String password) throws SQLException {
		
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call CAMBIAR_CONTRASINAL(?,?)}"); 
		
		miSentencia.setString(1, email);
		miSentencia.setString(2, password);
		

		miSentencia.execute();
		miSentencia.close();
		c.close();
	}
}
	
	

