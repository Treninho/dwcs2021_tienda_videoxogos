package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

/**
 * Clase ProductoDAO donde busca en la base de datos
 * @author Trenas
 *
 */
public class ProductoDAO {
/**
 * Atributos de clase ProductoDAO
 */
	private  ArrayList<Producto> productos;
	private  DataSource pool;
	
	
	/**
	 * Constructor de ProductoDAO 
	 * @param pool conexion a bdd
	 */
	public ProductoDAO (DataSource pool) {
		this.pool = pool;
	}
	
	
	public ProductoDAO() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Devuelve todos los productos de la BDD
	 * @return productos
	 * @throws Exception Lanza excepcion si no devuelve productos
	 */
	public  ArrayList<Producto> getAllProductos() throws Exception {
		Connection c = pool.getConnection();
		productos = new ArrayList<Producto>();
		PreparedStatement sentencia = c.prepareStatement("SELECT * FROM producto");
		ResultSet  rs= sentencia.executeQuery();
		while(rs.next()) {
			Producto producto = new Producto(rs.getInt("codigo"),rs.getString("nombre"), rs.getString("tipo"), rs.getDouble("prezo"), rs.getInt("unidades_stock"));
			productos.add(producto);
		}
		rs.close();
		c.close();
		return productos;
	}
	
	
	
	/**
	 * Elimina un producto
	 * @param codigoProducto elimina producto del codigo
	 * @param codigoCarrito codigo carrito
	 * @throws Exception si no se da eliminado un producto
	 */
	public void deleteProducto(int codigoProducto, int codigoCarrito) throws Exception{
		
		
		
		// eliminamos producto de carrito por si está alojado
		Connection c = pool.getConnection();
		CallableStatement  sentencia=c.prepareCall("{call DELETE_CARRITO(?)}"); 
		sentencia.setInt(1, codigoProducto);
		sentencia.execute();
		sentencia.close();
		
		// eliminamos producto de la lista
		CallableStatement  miSentencia=c.prepareCall("{call DELETE_PRODUCTO(?)}"); 
		miSentencia.setInt(1, codigoProducto);
		miSentencia.execute();
		miSentencia.close();
		c.close();
	}
	/**
	 * Inserta un producto nuevo
	 * @param nome inserta novo nome
	 * @param prezo inserta prezo
	 * @param stock inserta stock
	 * @param tipo inserta tipo
	 * @param descuento inserta descuento
	 * @throws Exception lanza excepcion si no da insertado nuevo prodcuto
	 */
	public void insertarProducto(String nome, double prezo, int stock, String tipo, int descuento ) throws Exception{
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call INSERTAR_PRODUCTO(?,?,?,?,?)}"); 
		miSentencia.setString(1, nome);
		miSentencia.setDouble(2, prezo);
		miSentencia.setInt(3, stock);
		miSentencia.setString(4, tipo);
		miSentencia.setInt(5, descuento);
		
		miSentencia.execute();
		miSentencia.close();
		c.close();
	}
	/**
	 * Edita un producto guardado
	 * @param codigo codigo producto
	 * @param nome nome producto
	 * @param prezo prezo produzcto
	 * @param stock stock producto
	 * @param tipo tipo producto
	 * @param descuento descuento producto
	 * @throws SQLException lanza exception
	 */
	public void editarProducto(int codigo, String nome, double prezo, int stock, String tipo, int descuento) throws SQLException {
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call EDITAR_PRODUCTO(?,?,?,?,?,?)}"); 
		miSentencia.setInt(1, codigo);
		miSentencia.setString(2, nome);
		miSentencia.setDouble(3, prezo);
		miSentencia.setInt(4, stock);
		miSentencia.setString(5, tipo);
		miSentencia.setInt(6, descuento);
		
		
		miSentencia.execute();
		miSentencia.close();
		c.close();
	}
	/**
	 * Añade un producto al carrito
	 * @param codigo codigo producto
	 * @param importe importe producto
	 * @param nome nome producto
	 * @throws SQLException sql exception
	 */
	public void putCarrito(int codigo,double importe, String nome) throws SQLException {
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call AÑADIR_CARRITO(?,?,?)}");
		miSentencia.setInt(1, codigo);
		miSentencia.setDouble(2, importe);
		miSentencia.setString(3, nome);
		
		
		miSentencia.execute();
		miSentencia.close();
		c.close();
	}
}
