package modelo;
/**
 * Clase CarritoProductos
 * @author Trenas
 *
 */
public class CarritoProductos {

	
	private int codigoCarrito;
	private int unidades;
	private double importe;
	private String nome;
	
	
	/**
	 * 
	 * @return nome Devuelve el nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Cambia el nombre
	 * @param nome del producto dentro del carrito
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

/**
 * Constructor de carrito 
 * @param codigoCarrito codigo del carrito
 * @param importe del producto
 * @param nome del producto
 */
	public CarritoProductos(int codigoCarrito,double importe, String nome) {
		super();
		this.codigoCarrito = codigoCarrito;
		this.importe = importe;
		this.nome = nome;
	}
	
	/**
	 * Devuelve el codigo del carrito
	 * @return el codigo del carrito
	 */
	public int getCodigoCarrito() {
		return codigoCarrito;
	}
	/**
	 * Cambia el codigo del carrito
	 * @param codigoCarrito codigo carrito
	 */
	public void setCodigoCarrito(int codigoCarrito) {
		this.codigoCarrito = codigoCarrito;
	}
	/**
	 * Devuelve las unidades
	 * @return unidades
	 */
	public int getUnidades() {
		return unidades;
	}
	/**
	 * 
	 * @param unidades Cambia las unidades
	 */
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	/**
	 * Devuelve el importe
	 * @return importe carrito
	 */
	public double getImporte() {
		return importe;
	}
	/**
	 * Cambia el importe
	 * @param importe carrito
	 */
	public void setImporte(double importe) {
		this.importe = importe;
	}
	
	
	
}
