package modelo;

import java.awt.List;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;



public class Producto {

	/**
	 * Atributos private de la clase producto
	 */
	// atributos de la clase Producto
	private String nome;
	private int codigoProducto;
	private String tipoProducto;
	private double prezo;
	private int unidades_stock;
	private double descuento_porciento;
	
	
	

	public void setDescuento_porciento(double descuento_porciento) {
		this.descuento_porciento = descuento_porciento;
	}

		/**
		 * Constructor Producto
		 * @param codigoProducto codigo del producto
		 * @param nome nome producto
		 * @param tipoProducto tipo producto
		 * @param prezo prezo producto
		 * @param unidades_stock stock
		 */
		public Producto (int codigoProducto,String nome, String tipoProducto, double prezo, int unidades_stock){
			this.codigoProducto =codigoProducto;
			this.nome = nome;
			this.tipoProducto = tipoProducto;
			this.prezo = prezo;
			this.unidades_stock = unidades_stock;
		}
	
		
		/**
		 * Método que devuelve el nombre actual del producto
		 * @return nome
		 */
		public String getNome() {
			return nome;
		}
		/**
		 * Método que modifica el nombre del producto
		 * @param nome nome producto
		 */
		public void setNome(String nome) {
			this.nome = nome;
		}
		/**
		 * Método que devuelve el código del producto
		 * @return codigoProducto
		 */
		@XmlAttribute
		public int getCodigoProducto() {
			return codigoProducto;
		}
		/**
		 * Método que modifica el codigo del producto
		 * @param codigoProducto codigo del producto
		 */
		public void setCodigoProducto(int codigoProducto) {
			this.codigoProducto = codigoProducto;
		}
		/**
		 * Método que devuelve el tipo de producto
		 * @return tipoProducto
		 */
		public String getTipoProducto() {
			return tipoProducto;
		}
		/**
		 * Método que modifica el tipo de producto
		 * @param tipoProducto tipo producto
		 */
		public void setTipoProducto(String tipoProducto) {
			this.tipoProducto = tipoProducto;
		}
		/**
		 * Método que devuelve el precio actual
		 * @return prezo prezo actual
		 */
		public double getPrezo() {
			return prezo;
		}
		/**
		 * Método que modifica el prezo
		 * @param prezo prezo producto
		 */

		public void setPrezo(double prezo) {
			this.prezo = prezo;
		}



		public int getUnidades_stock() {
			return unidades_stock;
		}
		public void setUnidades_stock(int unidades_stock) {
			this.unidades_stock = unidades_stock;
		}
		@Override
		public String toString() {
			return "Producto [nome=" + nome + ", tipoProducto=" + tipoProducto + ", prezo=" + prezo + "]";
		}
		 
		public double getDescuento_porciento() {
		return descuento_porciento;
	}
		
		/**
		 * Método que devuelve los datos actuales de la clase Producto.
		 */

	
	
}
