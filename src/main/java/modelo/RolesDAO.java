package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;
/**
 * Clase RolesDAO
 * @author Trenas
 *
 */
public class RolesDAO {

	/**
	 * Atributos
	 */
	private DataSource pool;
	private ArrayList<Roles> roles;
	
	/**
	 * Constructor de RolesDao
	 * @param pool
	 */
	public RolesDAO(DataSource pool) {
		super();
		this.pool = pool;
	}

/**
 * Devuelve todos los roles
 * @return roles
 */
	public ArrayList<Roles> getRoles() {
		return roles;
	}

	/**
	 * Cambia los roles
	 * @param roles
	 */
	public void setRoles(ArrayList<Roles> roles) {
		this.roles = roles;
	}
	
	/**
	 * Devuelve todos los roles de la Base de Datos
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Roles> getAllRoles() throws SQLException{
		Connection c = pool.getConnection();
		roles = new ArrayList<Roles>();
		PreparedStatement sentencia = c.prepareStatement("SELECT * FROM roles");
		ResultSet  rs= sentencia.executeQuery();
		while(rs.next()) {
			Roles rol = new Roles(rs.getString("rol"),rs.getString("email_cliente"));
			roles.add(rol);
		}
		rs.close();
		c.close();
		return roles;
	}
	/**
	 * Asigna un rol a un Cliente
	 * @param rol
	 * @param email
	 * @throws SQLException
	 */
	public void asignarRol (String rol, String email) throws SQLException {
		Connection c = pool.getConnection();
		CallableStatement  miSentencia=c.prepareCall("{call ASIGNAR_ROL(?,?)}"); 
		miSentencia.setString(1, "rol");
		miSentencia.setString(2, "email");
		miSentencia.execute();
		c.close();
		
	}
	
}
