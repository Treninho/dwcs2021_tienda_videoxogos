<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
     <%@ include file="header.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listado Productos</title>
<style>
	table, td {padding: 10px; font-family: arial;border: collapse; }
	table {
		border: 3px solid blue; 
		border-radius: 10px 10px;
		
	}
	.caja5 {
		margin:30px;
		float:left;
	}
	.caja1 {
		float:left;
		margin: 35px;
		
	}
	.caja2 {
		float:left;
		margin: 35px;
		
	}
	.caja3 {
		float:left;
		margin: 35px;
	}
	.caja4 {
		float:left;
		margin: 100px;
	}
	.titulo {
		font-size: 21px;
		font-family: arial;
		padding: 5px;
		background-color: lightblue;
		text-align: center;
		font-weight: bold;
	}
	.cent {
		display:block; 
		margin: auto;
	}
	.cabezado {
		font-size: 24px;
		font-family:arial;
		background-color: ligthblue;
	}
</style>
</head>
<body>
<p class="titulo">ZONA ADMINISTRACI�N</p>
<p>User: ${sessionScope.user}</p>
        <p>Roles: ${sessionScope.rol}</p>
<div class="caja1">
 	<p class="titulo">Listado Productos</p>
 	<table>
 		<tr class="cabecera">
		<td>C�digo</td>
		<td>Nome</td>
		<td>Tipo</td>
		<td>Prezo</td>
	</tr>
	<c:forEach var="producto" items="${lista}">
		<tr>
			<td>${producto.getCodigoProducto()}</td>
			<td>${producto.getNome()}</td>
			<td>${producto.getTipoProducto()}</td>
			<td>${producto.getPrezo()}</td>  
				<td><form method="post" action="/dwcs_tienda_videoxogos/controlador">
						<input type="hidden" name="id" value="${producto.getCodigoProducto()}">
						<input type="hidden" name="nomeP" value="${producto.getNome()}">
						<input type="hidden" name="tipoP" value="${producto.getTipoProducto()}">
						<input type="hidden" name="prezoP" value="${producto.getPrezo()}">
						<button type="submit" name="comando" value="insertarP">Insertar</button>
						<button type="submit" name="comando" value="editarP">Editar</button>
						<button type="submit" name="comando" value="eliminar">Eliminar</button>
					</form></td>
		</tr>
	</c:forEach>
	
	</table>
	
</div>
<c:if test="${param.comando=='insertarP'}">
<div class="caja2">
	<p class="titulo">Insertar Productos<p>	
<form method="post" action="/dwcs_tienda_videoxogos/controlador">
	<table>
		<tr>
			<td>Nome producto: </td>
			<td><input type="text" name="nProducto"></td>
		</tr>
		<tr>
			<td>Prezo:  </td>
			<td><input type="text" name="prezo"></td>
		</tr>
		<tr>
			<td>Stock:  </td>
			<td><input type="text" name="stock"></td>
		</tr>
		<tr>
			<td>Tipo:  </td>
			<td><input type="radio" name="tipo" value="consola">Consola <br>
				<input type="radio" name="tipo" value="videojuego">Videojuego</td>
		</tr>
		<tr>
			<td>Descuento:  </td>
			<td><input type="text" name="descuento"></td>
		</tr>
		<tr>
			<td></td>
			<td><button type="submit" name="comando" value="insertar">Insertar</td>
		</tr>
	</table>
	</form>
	</div>
</c:if>
<c:if test="${param.comando=='editarP'}">
	<div class="caja3">
	<p class="titulo">Editar Producto<p>
		<form method="POST" action="/dwcs_tienda_videoxogos/controlador">
	<table>
	<tr>
			<td>C�digo producto: </td>
			<td><input type="text" name="codigoEdit" value="${param.id}"></td>
		</tr>
		<tr>
			<td>Nome producto: </td>
			<td><input type="text" name="productoEdit" value="${param.nomeP}"></td>
		</tr>
		<tr>
			<td>Prezo:  </td>
			<td><input type="text" name="prezoEdit" value="${param.prezoP}"></td>
		</tr>
		<tr>
			<td>Stock:  </td>
			<td><input type="text" name="stockEdit"></td>
		</tr>
		<tr>
			<td>Tipo:  </td>
			<td><input type="radio" name="tipoEdit" value="consola">Consola <br>
				<input type="radio" name="tipoEdit" value="videojuego">Videojuego</td>
		</tr>
		<tr>
			<td>Descuento:  </td>
			<td><input type="text" name="descuentoEdit"></td>
		</tr>
		<tr>
			<td></td>
			<td><button type="submit" name="comando" value="confirm">Confirmar edici�n</button></td>
		</tr>
	</table>
	</form>
	</div>
</c:if>
	<br><br><br>


<div class="caja4">
<p class="titulo">Clientes</p>
 	<table>
 		<tr class="cabecera">
		<td>C�digo_Cliente</td>
		<td>Nome</td>
		<td>Apelidos</td>
		<td>Direccion</td>
		<td>Email</td>
	</tr>
	<c:forEach var="cliente" items="${clientes}">
		<tr>
			<td>${cliente.getCodigoCliente()}</td>
			<td>${cliente.getNome()}</td>
			<td>${cliente.getApelidos()}</td>
			<td>${cliente.getDireccion()}</td>
			<td>${cliente.getEmail()}</td>    
				<td><form method="post" action="/dwcs_tienda_videoxogos/controlador">
						<input type="hidden" name="idCliente" value="${cliente.codigoCliente}">
						<input type="hidden" name="emailCliente"value="${cliente.getEmail()}">
						<button type="submit" name="comando" value="a�adirRol">A�adir ROL</button>
						<button type="submit" name="comando" value="deleteCliente">Eliminar</button>
					</form></td>
		</tr>
	</c:forEach>
	</table>
</div>
<c:if test="${param.comando=='a�adirRol'}">
<div class="caja5">
	<p class="titulo">Asignar ROL</p>
	<form method="POST" action="/dwcs_tienda_videoxogos/controlador">
		<table>
		<tr>
			<td>Correo Electronico: </td>
			<td><input type="text" name="emailRol" value="${param.emailCliente}"></th>
		</tr>
		<tr>
			<td>Asignar rol</td>
			<td>ADMIN: <input type="radio" name="roles" value="admin"><br>
				USER: <input type="radio" name="roles" value="user"></td>
		</tr>
		<td colspan="2"><button type="submit" name="comando" value="asignar">Asignar</button></td>
		</table>
	</form>
</div>
</c:if>



</body>
</html>